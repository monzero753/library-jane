<?php
    session_start();

    date_default_timezone_set("Asia/Bangkok");
    #---------------------------#

    #---- Connect Database -----#
    $host = "localhost";
	$username = "root";
	$password = "password";
    $database = "library1";

    $mysqli_p = mysqli_connect($host, $username, $password, $database);
    if (!$mysqli_p) {
        die("Connection failed: " . mysqli_connect_error());
    }
    mysqli_set_charset($mysqli_p, "utf8");
    #---------------------------#

    $today = date("Y-m-d");
    $times = date("H:i:s");
?>