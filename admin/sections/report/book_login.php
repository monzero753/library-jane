<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">รายงานการเข้าใช้ห้องสมุด</h1></br>

<!-- check value from search -->
<?php
    $search_member_id = !empty($_POST["search_member_id"]) ? $_POST["search_member_id"] : '';
    $search_name = !empty($_POST["search_name"]) ? $_POST["search_name"] : '';
    $search_lname = !empty($_POST["search_lname"]) ? $_POST["search_lname"] : '';
    $search_class = !empty($_POST["search_class"]) ? $_POST["search_class"] : '';
    $search_room = !empty($_POST["search_room"]) ? $_POST["search_room"] : '';
    $search_date_start = !empty($_POST["search_date_start"]) ? $_POST["search_date_start"] : $today;
    $search_date_end = !empty($_POST["search_date_end"]) ? $_POST["search_date_end"] : $today;
?>

<!-- DataTales Search -->
<div class="card shadow mb-4">
    <div class="card-body">
        <form  method="post" id="frmsearch" name="frmsearch" action="./?mode=<?php echo $_GET["mode"]; ?>">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="search_member_id">รหัสนักเรียน</label>
                    <input type="text" class="form-control" name="search_member_id" id="search_member_id" value="<?php echo $search_member_id; ?>" />
                </div>
                <div class="col-md-4 mb-3">
                    <label for="search_name">ชื่อ</label>
                    <input type="text" class="form-control" name="search_name" id="search_name" value="<?php echo $search_name; ?>" />
                </div>
                <div class="col-md-4 mb-3">
                    <label for="search_lname">นามสกุล</label>
                    <input type="text" class="form-control" name="search_lname" id="search_lname" value="<?php echo $search_lname; ?>" />
                </div>
                <div class="col-md-3 mb-3">
                    <label for="search_class">ชั้น</label>
                    <input type="text" class="form-control" name="search_class" id="search_class" value="<?php echo $search_class; ?>" />
                </div>
                <div class="col-md-3 mb-3">
                    <label for="search_room">ห้อง</label>
                    <input type="text" class="form-control" name="search_room" id="search_room" value="<?php echo $search_room; ?>" />
                </div>
                <div class="col-md-3 mb-3">
                    <label for="search_date_start">วันที่ (เริ่ม)</label>
                    <input type="date" class="form-control" name="search_date_start" id="search_date_start" value="<?php echo $search_date_start; ?>" />
                </div>
                <div class="col-md-3 mb-3">
                    <label for="search_date_end">วันที่ (สิ้นสุด)</label>
                    <input type="date" class="form-control" name="search_date_end" id="search_date_end" value="<?php echo $search_date_end; ?>" />
                </div>
                <div class="col-md-6 md-3">
                    <button type="submit" class="btn btn-success">
                        <span class="icon text-white-50">
                        <i class="fas fa-search"></i>
                        </span>
                        <span class="text">&nbsp;ค้นหา</span>
                    </button>
                    <button type="button" class="btn btn-warning" onclick="window.location.href='./?mode=report/book_login'">
                        <span class="icon text-white-50">
                        <i class="fas fa-redo-alt"></i>
                        </span>
                        <span class="text">&nbsp;ล้างค่า</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <!-- <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
  </div> -->
  <?php 
    #---- Query TB_singinst ----#
    $singsql = "SELECT tb_singinst.*, tb_student.member_id as stuid , tb_student.prename as stupre ,
                tb_student.name_th as stuname , tb_student.lastname_th as stulname , tb_student.class as stuclass ,
                tb_student.room as sturoom
                FROM tb_singinst 
                LEFT JOIN tb_student
                    ON tb_singinst.member_id = tb_student.member_id
                WHERE tb_singinst.id > '0'
    ";
    if(!empty($search_member_id)){
        $singsql .= "AND tb_singinst.member_id = '".$search_member_id."' ";
    }
    if(!empty($search_name)){
        $singsql .= "AND tb_student.name_th = '".$search_name."' ";
    }
    if(!empty($search_lname)){
        $singsql .= "AND tb_student.lastname_th = '".$search_lname."' ";
    }
    if(!empty($search_class)){
        $singsql .= "AND tb_student.class = '".$search_class."' ";
    }
    if(!empty($search_room)){
        $singsql .= "AND tb_student.room = '".$search_room."' ";
    }
    if(!empty($search_date_start) && !empty($search_date_end)){
        $singsql .= "AND tb_singinst.date_sn BETWEEN '".$search_date_start."' AND '".$search_date_end."' ";
    }

    $singresult = mysqli_query($mysqli_p, $singsql);
    $i = 1;
  ?>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ลำดับ</th>
            <th>รหัสนักเรียน</th>
			<th>คำนำหน้า</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>ชั้น</th>
			<th>ห้อง</th>
            <th>วันที่</th>
            <th>เวลา</th>
          </tr>
        </thead>
        <tbody>
        <?php while($singrows = mysqli_fetch_array($singresult)){ ?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $singrows["member_id"];?></td>
            <td><?php echo $singrows["stupre"];?></td>
            <td><?php echo $singrows["stuname"];?></td>
            <td><?php echo $singrows["stulname"];?></td>
            <td><?php echo $singrows["stuclass"];?></td>
            <td><?php echo $singrows["sturoom"];?></td>
            <td><?php echo date("d-m-Y", strtotime($singrows["date_sn"]));?></td>
            <td><?php echo date("H:i", strtotime($singrows["time_sn"]));?></td>
          </tr>
        <?php $i++;
        } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->