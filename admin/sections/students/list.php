<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">รายชื่อนักเรียน</h1></br>

<!-- check value from search -->
<?php
    $search_member_id = !empty($_POST["search_member_id"]) ? $_POST["search_member_id"] : '';
    $search_name = !empty($_POST["search_name"]) ? $_POST["search_name"] : '';
    $search_lname = !empty($_POST["search_lname"]) ? $_POST["search_lname"] : '';
    $search_class = !empty($_POST["search_class"]) ? $_POST["search_class"] : '';
    $search_room = !empty($_POST["search_room"]) ? $_POST["search_room"] : '';
    $search_date_start = !empty($_POST["search_date_start"]) ? $_POST["search_date_start"] : $today;
    $search_date_end = !empty($_POST["search_date_end"]) ? $_POST["search_date_end"] : $today;
?>

<!-- DataTales Search -->
<div class="card shadow mb-4">
    <div class="card-body">
        <form  method="post" id="frmsearch" name="frmsearch" action="./?mode=<?php echo $_GET["mode"]; ?>">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="search_member_id">รหัสนักเรียน</label>
                    <input type="text" class="form-control" name="search_member_id" id="search_member_id" value="<?php echo $search_member_id; ?>" />
                </div>
                <div class="col-md-4 mb-3">
                    <label for="search_name">ชื่อ</label>
                    <input type="text" class="form-control" name="search_name" id="search_name" value="<?php echo $search_name; ?>" />
                </div>
                <div class="col-md-4 mb-3">
                    <label for="search_lname">นามสกุล</label>
                    <input type="text" class="form-control" name="search_lname" id="search_lname" value="<?php echo $search_lname; ?>" />
                </div>
                <div class="col-md-3 mb-3">
                    <label for="search_class">ชั้น</label>
                    <input type="text" class="form-control" name="search_class" id="search_class" value="<?php echo $search_class; ?>" />
                </div>
                <div class="col-md-3 mb-3">
                    <label for="search_room">ห้อง</label>
                    <input type="text" class="form-control" name="search_room" id="search_room" value="<?php echo $search_room; ?>" />
                </div>
                <div class="col-md-3 mb-3">
                    
                </div>
                <div class="col-md-3 mb-3">
                
                </div>
                <div class="col-md-6 md-3">
                    <button type="submit" class="btn btn-success">
                        <span class="icon text-white-50">
                        <i class="fas fa-search"></i>
                        </span>
                        <span class="text">&nbsp;ค้นหา</span>
                    </button>
                    <button type="button" class="btn btn-warning" onclick="window.location.href='./?mode=students/list'">
                        <span class="icon text-white-50">
                        <i class="fas fa-redo-alt"></i>
                        </span>
                        <span class="text">&nbsp;ล้างค่า</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

 <!-- manu Content Row -->
<div class="row">

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="h5 font-weight-bold text-primary text-uppercase mb-1">เพิ่มข้อมูลนักเรียน (.VCS)</div>
            <div class="mb-0 text-gray-800">
               <form action="./?mode=students/process_uploadstu" method="post" enctype="multipart/form-data" name="form1">
                  <input name="fileCSV" type="file" id="fileCSV">
                  <button type="submit" class="btn btn-success">
                          <span class="text">ยืนยัน</span>
                  </button>
                </form>
            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-calendar fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="h5 font-weight-bold text-success text-uppercase mb-1">เลื่อนชั้น (ปีการศึกษา)</div>
            <div class="mb-0 text-gray-800">
              <form action="./?mode=students/process_upclassstu" method="post" enctype="multipart/form-data" name="form1">
                <div class="row">
                  <div class="col-md-6 mb-6">
                    <select  class="form-control" name="class" id="class">
                      <option value="1">ม.1</option>
                      <option value="2">ม.2</option>
                      <option value="3">ม.3</option>
                      <option value="4">ม.4</option>
                      <option value="5">ม.5</option>
                    </select>
                  </div>
                  <div class="col-md-6 mb-6">
                    <button type="submit" class="btn btn-success">
                      <span class="text">ยืนยัน</span>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="้5 font-weight-bold text-info text-uppercase mb-1">ลบนักเรียนจบการศึกษา</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
              </div>
              <div class="col">
                <div class="progress progress-sm mr-2">
                  <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- manu Content Row -->

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <!-- <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
  </div> -->
  <?php 
    #---- Query TB_singinst ----#
    $stusql = "SELECT * from tb_student
    ";
    // if(!empty($search_member_id)){
    //     $singsql .= "AND tb_singinst.member_id = '".$search_member_id."' ";
    // }
   
    $sturesult = mysqli_query($mysqli_p, $stusql);
    $i = 1;
  ?>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ลำดับ</th>
            <th>รหัสนักเรียน</th>
			<th>คำนำหน้า</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>ชั้น</th>
			<th>ห้อง</th>
            <th>เพศ</th>
            <th>รูป</th>
            <th>แก้ไข/ลบ</th>
          </tr>
        </thead>
        <tbody>
        <?php while($sturows = mysqli_fetch_array($sturesult)){ ?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $sturows["member_id"];?></td>
            <td><?php echo $sturows["prename"];?></td>
            <td><?php echo $sturows["name_th"];?></td>
            <td><?php echo $sturows["lastname_th"];?></td>
            <td><?php echo $sturows["class"];?></td>
            <td><?php echo $sturows["room"];?></td>
            <td><?php echo $sturows["sex"];?></td>
            <td> <img src="../images/member/<?php echo $sturows["picture"]; ?>" alt="Paris" width="15%"></td>
            <td>
            <a href="#" class="btn btn-warning btn-circle btn-sm">
                     <i class="fas fa-pencil-alt"></i>
            </a>
            <a href="#" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
            </a>       
            </td>
          </tr>
        <?php $i++;
        } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->