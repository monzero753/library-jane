<?php
    require "../inc/connection.php";
    if(empty($_SESSION["student"]["id"])){
        echo "<script type='text/javascript'>"; 
        echo "alert('กรุณาเข้าสู่ระบบ');"; 
        echo "window.location = 'login.php'; "; 
        echo "</script>";
    }

    #--- Query tb_student ---#
    $stusql = "SELECT * FROM tb_student WHERE member_id = '".$_SESSION["student"]["id"]."' ";
    $sturesult = mysqli_query($mysqli_p, $stusql);
    $sturows = mysqli_fetch_array($sturesult);

    #--- Query tb_student ---#
    $stusql = "SELECT DISTINCT date_sn FROM tb_singinst WHERE member_id = '".$_SESSION["student"]["id"]."' ORDER BY date_sn ASC ";
    $sturesult = mysqli_query($mysqli_p, $stusql);
    $stunum = mysqli_num_rows($sturesult);

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Library - Login</title>


  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-3">
                <img src="../images/member/<?php echo $sturows["picture"]; ?>" alt="Paris" width="234" height="324">
              </div> <!-- รูปภาพ -->
              <div class="col-lg-9">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">ลงชื่อเข้าใช้งานห้องสมุด</h1>
                  </div>
                  <div class="text-gray-900">
                    <p class="mb-4">ชื่อ - <?php echo $sturows["prename"]." ".$sturows["name_th"]." ".$sturows["lastname_th"]; ?></br>
                    ชั้น - <?php echo $sturows["class"]."/".$sturows["room"]; ?></br>
                    สถิติการเข้าสู่ระบบ - <?php echo $stunum; ?> ครั้ง
                  </p>
                  </div>
                  <hr>
                  <form class="user">
                    <a href="login.php" class="btn btn-primary btn-user btn-block">
                      Login Page
                    </a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

</body>

</html>